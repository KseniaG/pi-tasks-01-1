#include <stdio.h>
int main()
{
	int foot = 0, inches = 0;
	float cm;
	printf("Enter your height in the format foot'inches\n");
	scanf("%d'%d", &foot, &inches);
	if (foot < 0 || inches < 0)
	{
		puts("Input error!");
		return 1;
	}
	cm = (foot*12 + inches)*2.54;
	printf("Your height %0.2f cm\n", cm);
	return 0;
}

