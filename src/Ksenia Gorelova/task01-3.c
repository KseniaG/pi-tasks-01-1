#include <stdio.h>
int main()
{
	char str[256];
	int i = 0, number = 0, sum = 0;
	puts("Enter a string \n");
	fgets(str, 256, stdin);
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
		{
			number = number*10 + (str[i] - '0');
			i++;
		}
		else if (str[i] < '0' || str[i] > '9')
		{
			sum += number;
			number = 0;
			i++;
		}
	}
	printf("Sum numbers in the string = %d \n", sum);
	return 0;
}